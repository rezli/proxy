const express = require("express");

const {
  createConnectionRecord,
  getConnectionRecords,
  admins,
} = require("./authentication.model.js");

const router = express.Router();

router.post("/record", createConnectionRecord);
router.get("/getRecords", getConnectionRecords);

exports.router = router;