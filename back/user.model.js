const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please State Your Name"],
  },
  email: {
    type: String,
    required: [true, "Please Provide Your Email"],
    unique: [true, "Email Used Before"],
    lowercase: true,
    // validate: ".لطفا ایمیل خود را به درستی وارد کنید",
  },
  password: {
    type: String,
    required: [true, "Please provide a password"],
    minlength: [4, "Your password must be more than 8 characters"],
    select: false,
  },
  passwordChangedAt: Date,
  passwordResetToken: String,
  passwordResetExpires: Date,
});

const User = mongoose.model("User", userSchema);

exports.User = User;