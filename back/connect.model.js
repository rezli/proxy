const mongoose = require("mongoose");

const connectionsSchema = new mongoose.Schema({
  host: {
    type: String,
  },
  port: {
    type: Number,
  },
  bytesWritten: {
    type: Number,
  },
  status: {
    type: String,
  }
});

const Connection = mongoose.model("Connection", connectionsSchema);

exports.Connection = Connection;