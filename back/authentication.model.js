const { User } = require("./user.model.js");
const { Connection } = require("./connect.model.js");
const { Errors , APIFeatures , AppError } = require("./utils.js");
const jwt = require("jsonwebtoken");
const { promisify } = require("util");

const catchAsync = (fn) => {
  return (req, res, next) => {
    fn(req, res, next).catch(next);
  };
};

const createConnectionRecord = catchAsync(async (req, res, next) => {
  const connect = await Connection.create(req.body);
  console.log("\n connection Recorded!");
});

const getAll = (Model) =>
  catchAsync(async (req, res, next) => {
    // To allow for nested GET reviews on tour (hack)
    let filter = {};
    const collectionCount = await Model.countDocuments();
    const features = new APIFeatures(Model.find(filter), req.query)
      .filter()
      .sort()
      .limitFields()
      .paginate()
      .populate();

    // const docs = await features.query.explain();
    const docs = await features.query;
    console.log('INJA!')
    // Send Response
    res.status(200).json({
      status: "success",
      collectionCount,
      results: docs.length,
      data: { data: docs },
    });
  });

exports.getConnectionRecords = getAll(Connection);
exports.createConnectionRecord = createConnectionRecord;