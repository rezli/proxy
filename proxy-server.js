const net = require('net');
const http = require("http");
const mongoose = require("mongoose");
const morgan = require("morgan");
const path = require("path");
const express = require("express");
const cors = require("cors")
const { router } = require("./back/route.js");

const server = net.createServer();


const PostRequest = (body) => {
  console.log("\n Socket Detail", body);
  const data = JSON.stringify(body);
  const options = {
    host: "127.0.0.1",
    port: 5000,
    path: "/api/v1/users/record",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": data.length,
    },
  };

  const req = http.request(options, (res) => {
    console.log(`statusCode: ${res.statusCode}`);

    res.on("data", (d) => {
      process.stdout.write(d);
    });
  });

  req.on("error", (error) => {
    console.error(error);
  });

  req.write(data);
  req.end();
};
const proxy = () => {

server.on('connection', (socket) => connectionHandler(socket));

server.on('error', (err) => {
  console.log('UNEXPECTED SERVER ERROR');
  console.log(err);
  throw err;
});

server.on('close', () => {
  console.log('Server Disconnected');
});

server.listen(8000, () => {
  console.log(`Proxy Server runnig on http://localhost:${8000}`);
});

const connectionHandler = (socket) => {
  console.log('Connected To Proxy');

  socket.once('data', (data) => socketDataHandler(socket, data));
};

const socketDataHandler = (socket, data) => {
  const message = data.toString();
  const mainHeader = message.split('\n')[0];
  let port = 80;
  let host = '';
  const isConnect = data.toString().startsWith('CONNECT ');
  console.log(isConnect);

  if (isConnect) {
    console.log('CONNECT');
    const [, url] = mainHeader.split(' ');

    [host, port] = url.split(':');
  } else {
    console.log('NOT CONNECT');
    host = message.split('Host: ')[1].split('\r\n')[0];
    const isIp = host.split(':');
    if (isIp.length > 1) {
      host = isIp[0];
      port = isIp[1];
    }
  }

  port = Number(port);
  console.log({ host, port });

  const reponseSocket = net.createConnection({ host, port }, () => {
    console.log('PROXT To REMOTE');

    if (isConnect) {
      socket.write('HTTP/1.1 200 OK\r\n\n');
    } else {
      reponseSocket.write(data);
    }

    reponseSocket.pipe(socket);
    socket.pipe(reponseSocket);
      PostRequest({
        host : host ,
        port : 8000,
        bytesWritten : socket.bytesRead,
        status : 'success'
      })
    reponseSocket.on('error', (err) => {
      console.log('Proxy TO REMOTE ERROR');
      console.log(err);
    });
    reponseSocket.on('close', () => {
      console.log(`Proxy TO Server ${host} Hanged Up!`);
      console.log(socket.bytesRead);
      console.log(socket.bytesWritten);
    });
    socket.on('error', (err) => {
      console.log('Client TO PROXY ERROR');
      console.log(err);
    });
    socket.on('close', () => {
      console.log(`Client TO PROXY Socket Hanged Up! ${host}`);
    });
  });
};
};

process.on("uncaughtException", (err) => {
  console.log("UNCAUGHT_EXCEPTION! Shutting down...");
  console.log(err.name, err.message);
  process.exit(1);
});


const app = express();

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(express.json());

app.use(cors());

const PORT = 5000;
app.use(express.static(path.resolve(__dirname , './admin/build')))

app.use(morgan("dev"));

const dashboardServer = () => {
  const instanceOfServer = app.listen(PORT, () => {
    app.use("/api/v1/users", router);
    app.get("*", (req, res, next) => {
      res.sendFile(path.join(__dirname,'./admin/build' , 'index.html'));
    });


    console.log(
      "%c%s",
      "color: green;",
      `Dashboard Server Running on http://localhost:${PORT} \n`
    );
  });
  instanceOfServer.on("close", () => {
    mongoose.connection.close();
    console.log("server shutting down");
  });
  return instanceOfServer;
};


const DBconnect = () => {
  return mongoose
    .connect('mongodb+srv://<username>:<password>@cluster0.iyfla.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
       , {
      auth: {
        user: 'dbUser',
        password: 'gsGU8nv8DJrEz9y'
      },
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      poolSize: 10,
    })
    .then(() => {
      dashboardServer();
    })
    .catch((e) => {
      console.error(
        "Failed to connect to mongo on startup - retrying in 5 sec",
        e
      );
      mongoose.connection.close();
      dashboardServer().close();
   
    });
};

mongoose.connection.on("close", () => {
  console.log("DB Disconnected Successfully");
});


process.on("unhandledRejection", (err) => {
  console.log(err.name, err.message);
  console.log("UNHANDLED_REJECTION!   Shutting down...");
  dashboardServer.close(() => {
    process.exit(1);
  });
});

DBconnect();

proxy();