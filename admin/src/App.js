import React, { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios';

function App() {
  const [loading, setloading] = useState(true);
  const [data, setData] = useState(null);

  useEffect(() => {
    const datafunc =  () => {
      
      setloading(true)
      axios('http://localhost:5000/api/v1/users/getRecords/')
        .then(data => { setloading(false); setData(data.data.data.data); })
        .catch(e => console.log('error', e.message))

    };

    datafunc();

  }, []);

  useEffect(() => console.log('data' , data) , [data])

  return (
    <div className="App">
      <header className="App-header">
        Proxy Server
      </header>
      {
      loading ? 'loading ...' : (
          <article className="table-container">
            <table className="table">
              <tr>
                <th>host</th>
                <th>status</th>
                <th>port</th>
                <th>bytesWritten</th>
              </tr>
              { data  && data.map((i, index) => (
                <tr key={index}>
                  <td>
                    {i.host}
                  </td>
                  <td>
                    {i.status}
                  </td>
                  <td>
                    {i.port}
                  </td>
                  <td>
                    {i.bytesWritten}
                  </td>
                </tr>
              ))
              }
            </table>
          </article>
        )
      }

    </div>
  );
}

export default App;
